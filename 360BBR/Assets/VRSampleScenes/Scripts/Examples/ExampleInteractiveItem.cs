using UnityEngine;
using VRStandardAssets.Utils;


    // This script is a simple example of how an interactive item can
    // be used to change things on gameobjects by handling events.
    public class ExampleInteractiveItem : MonoBehaviour
    {
        [SerializeField] private Material m_NormalMaterial;                
        [SerializeField] private Material m_OverMaterial;                  
        [SerializeField] private Material m_ClickedMaterial;               
        [SerializeField] private Material m_DoubleClickedMaterial;         
        [SerializeField] private VRInteractiveItem m_InteractiveItem;
        [SerializeField] private Renderer m_Renderer;


		[SerializeField] public Material p_NormalMaterial;                
		[SerializeField] public Material p_OverMaterial;                  
		[SerializeField] public Material p_ClickedMaterial;               
		[SerializeField] public Material p_DoubleClickedMaterial;         
		[SerializeField] public VRInteractiveItem p_InteractiveItem;
		[SerializeField] public Renderer p_Renderer;
		public bool show=false;
	    public static bool overbone=false;
        public void Awake ()
        {
            m_Renderer.material = m_NormalMaterial;
			p_Renderer.material = p_NormalMaterial;
        }


       public void OnEnable()
        {
            m_InteractiveItem.OnOver += HandleOver;
            m_InteractiveItem.OnOut += HandleOut;
            m_InteractiveItem.OnClick += HandleClick;
            m_InteractiveItem.OnDoubleClick += HandleDoubleClick;

        }


        public void OnDisable()
        {
            m_InteractiveItem.OnOver -= HandleOver;
            m_InteractiveItem.OnOut -= HandleOut;
            m_InteractiveItem.OnClick -= HandleClick;
            m_InteractiveItem.OnDoubleClick -= HandleDoubleClick;

        }


        //Handle the Over event
        public void HandleOver()
        {
            Debug.Log("Show over state");
		    overbone = true;
		    print (m_InteractiveItem +"is triggered");
            m_Renderer.material = m_OverMaterial;
			p_Renderer.material = p_OverMaterial;

        }


        //Handle the Out event
        public void HandleOut()
        {
            Debug.Log("Show out state");
		    overbone = false;
            m_Renderer.material = m_NormalMaterial;
			p_Renderer.material = p_NormalMaterial;

        }


        //Handle the Click event
        public void HandleClick()
        {
			show = !(show);
            Debug.Log("Show click state");
            m_Renderer.material = m_ClickedMaterial;
			if(show)
			p_Renderer.material = p_ClickedMaterial;
		    else
			p_Renderer.material = p_NormalMaterial;
        }


        //Handle the DoubleClick event
        public void HandleDoubleClick()
        {
            Debug.Log("Show double click");
            m_Renderer.material = m_DoubleClickedMaterial;
			p_Renderer.material = p_DoubleClickedMaterial;
        }
    }

