using UnityEngine;
using VRStandardAssets.Utils;


    // This script is a simple example of how an interactive item can
    // be used to change things on gameobjects by handling events.
    public class ExampleInteractiveItemglow : MonoBehaviour
    {
        [SerializeField] private Material m_NormalMaterial;                
        [SerializeField] private Material m_OverMaterial;                  
        [SerializeField] private Material m_ClickedMaterial;               
        [SerializeField] private Material m_DoubleClickedMaterial;         
        [SerializeField] private VRInteractiveItem m_InteractiveItem;
        [SerializeField] private Renderer m_Renderer;

		private bool show=false;
		public  static bool over=false;


        public void Awake ()
        {
            m_Renderer.material = m_NormalMaterial;

        }


       public void OnEnable()
        {
            m_InteractiveItem.OnOver += HandleOver;
            m_InteractiveItem.OnOut += HandleOut;
            m_InteractiveItem.OnClick += HandleClick;
            m_InteractiveItem.OnDoubleClick += HandleDoubleClick;

        }


        public void OnDisable()
        {
            m_InteractiveItem.OnOver -= HandleOver;
            m_InteractiveItem.OnOut -= HandleOut;
            m_InteractiveItem.OnClick -= HandleClick;
            m_InteractiveItem.OnDoubleClick -= HandleDoubleClick;

        }


        //Handle the Over event
        public void HandleOver()
        {
            Debug.Log("Show over state");
			over = true;
            m_Renderer.material = m_OverMaterial;


        }


        //Handle the Out event
        public void HandleOut()
        {
			over = false;
            Debug.Log("Show out state");
            m_Renderer.material = m_NormalMaterial;


        }


        //Handle the Click event
        public void HandleClick()
        {
			show = !(show);
            Debug.Log("Show click state");
			if(show)
            m_Renderer.material = m_ClickedMaterial;
		
        }


        //Handle the DoubleClick event
        public void HandleDoubleClick()
        {
            Debug.Log("Show double click");
            m_Renderer.material = m_DoubleClickedMaterial;

        }
    }

