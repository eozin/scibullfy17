﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class blink1 : MonoBehaviour {
	public Renderer rend;
	[SerializeField] private Material m_NormalMaterial;  
	[SerializeField] private Material m_OverMaterial;      
	public float fYRot;
	public bool Overbone;
	void Start() {
		rend = GetComponent<Renderer>();
		rend.material.shader = Shader.Find("MK/Glow/Selective/Mobile/Diffuse");
	}
	void Update() {
		Overbone = ExampleInteractiveItem.overbone;
		//GameObject go = GameObject.Find ("bone/default/default_MeshPart0");
		//ExampleInteractiveItemglow speedController = go.GetComponent <ExampleInteractiveItem> ();
		if (!Overbone) {
			//print (Overbone);

			fYRot = Camera.main.transform.eulerAngles.y;
			if ((fYRot > 200.0) && (fYRot < 270.0)) {
				//bool over = ExampleInteractiveItemglow.over;
				//if (over) {
				rend.material = m_OverMaterial;    
				float shininess = Mathf.PingPong (Time.time, 1.0F);
				rend.material.SetFloat ("_MKGlowPower", shininess);
				//} 

			} else {
				rend.material = m_NormalMaterial;
				//rend.material.SetFloat ("_MKGlowPower", 0);
			}
		} else {
			rend.material = m_NormalMaterial;
		}
	}
}