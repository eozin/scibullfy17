﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class blink3 : MonoBehaviour {
	public Renderer rend;
	[SerializeField] private Material m_NormalMaterial;  
	[SerializeField] private Material m_OverMaterial;      
	float fYRot;
	public bool Overbone;
	void Start() {
		rend = GetComponent<Renderer>();
		rend.material.shader = Shader.Find("MK/Glow/Selective/Mobile/Diffuse");
	}
	void Update() {
		//float currentSpeed = ExampleInteractiveItemglow.SharedInstance.speed;
		//GameObject go = GameObject.Find ("bone/default/default_MeshPart0");
		//ExampleInteractiveItemglow speedController = go.GetComponent <ExampleInteractiveItem> ();
		Overbone = ExampleInteractiveItem.overbone;

		if (!Overbone) {
			fYRot = Camera.main.transform.eulerAngles.y;
			if ((fYRot > 300.0) && (fYRot < 360.0)) {
				//bool over = ExampleInteractiveItemglow.over;
				//if (over) {
				rend.material = m_OverMaterial;    
				float shininess = Mathf.PingPong (Time.time, 1.0F);
				rend.material.SetFloat ("_MKGlowPower", shininess);
				//} 

			} else {
				rend.material = m_NormalMaterial;
				//rend.material.SetFloat ("_MKGlowPower", 0);
			} 
		}else {
		rend.material = m_NormalMaterial;
	    }
	}
}