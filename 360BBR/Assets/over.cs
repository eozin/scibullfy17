﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class over : MonoBehaviour {
	public Material m_NormalMaterial;                
	public Material m_OverMaterial;                         
	public Renderer rend;
	//public static Renderer rend = GetComponent<Renderer>();

	float fYRot;
	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer>();

	}
	
	// Update is called once per frame
	void Update () {
		fYRot = Camera.main.transform.eulerAngles.y;
		if ((fYRot>300.0) && (fYRot<360.0))
			rend.material = m_OverMaterial;
		else
			rend.material = m_NormalMaterial;
	}
		

}

