﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpTest : MonoBehaviour {

	public Transform startM;
	public Transform endM;
	public float speed = 1.0f;
	public float rotSpeed = 2.5f;

	private float startTime;
	private float journeyLength;

	// Use this for initialization
	void Start () {
		startTime = Time.time;
		journeyLength = Vector3.Distance (startM.position, endM.position);

	}
	
	// Update is called once per frame
	void Update () {
		float distCovered = (Time.time - startTime) * speed;
		float fracJourney = distCovered / journeyLength;
		transform.position = Vector3.Lerp (startM.position, endM.position, fracJourney);

		//rotation
		 Vector3 targetDir = endM.position - transform.position;
		 float step = rotSpeed * Time.deltaTime;
		 Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0F);
       	 Debug.DrawRay(transform.position, newDir, Color.red);
//         transform.rotation = Quaternion.LookRotation(newDir);

		transform.Rotate ( new Vector3(0f, 0f, -1.5f) );
		 
	}

}
