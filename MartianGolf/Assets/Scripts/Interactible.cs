﻿using UnityEngine;

/// <summary>
/// Conveys the set of states available on an Interactible.
/// In the inspector check the states to display on each Interactible.
/// </summary>
[System.Serializable]
public class InteractibleParameters
{
    public bool Scrollable = true;
    public bool Placeable = true;
}

/// <summary>
/// The Interactible class flags a Game Object as being "Interactible".
/// Determines what happens when an Interactible is being gazed at.
/// </summary>
public class Interactible : MonoBehaviour
{
    public InteractibleParameters InteractibleParameters;

    [Tooltip("Audio clip to play when interacting with this hologram.")]
    public AudioClip TargetFeedbackSound;
    private AudioSource audioSource;

    void Start()
    {
        // Add a BoxCollider if the interactible does not contain one.
        Collider collider = GetComponentInChildren<Collider>();
        if (collider == null)
        {
            gameObject.AddComponent<BoxCollider>();
        }

        EnableAudioHapticFeedback();
    }

    private void EnableAudioHapticFeedback()
    {
        // If this hologram has an audio clip, add an AudioSource with this clip.
        if (TargetFeedbackSound != null)
        {
            audioSource = GetComponent<AudioSource>();
            if (audioSource == null)
            {
                audioSource = gameObject.AddComponent<AudioSource>();
            }

            audioSource.clip = TargetFeedbackSound;
            audioSource.playOnAwake = false;
            audioSource.spatialBlend = 1;
            audioSource.dopplerLevel = 0;
        }
    }

	void GazeEntered()
	{
		/*
        for (int i = 0; i < defaultMaterials.Length; i++)
        {
            defaultMaterials[i].SetFloat("_Highlight", .25f); //white 
			if(this.name == "Shortfin_Mako_Shark_body"){
				//defaultMaterials[i].SetColor("_Color", Color.red); //blue
				defaultMaterials[i].SetColor("_Color", new Color(0.17f, 0.7f, 1f, 0.31f)); //blue
			}
        }
		*/

		// Play the audioSource feedback when we gaze and select a hologram.
		if (audioSource != null && !audioSource.isPlaying)
		{
			audioSource.Play();
		}

		//		this.GetComponent<SpriteRenderer>().color = new Color(0.25f, 1, 0.25f, 1);
	}

	void GazeExited(){

	}

	void LateUpdate()
	{
		Debug.ClearDeveloperConsole();
	}
}