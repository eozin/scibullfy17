﻿using UnityEngine;
using System.Collections;

public class WhatAngle : MonoBehaviour {

	TextMesh angle; 

	void Start () {
		angle = gameObject.GetComponentInChildren<TextMesh> ();
		angle.text = "";
	}


	void Update () {
		angle.text = ((int)gameObject.transform.localEulerAngles.y).ToString() + "°";
	}
}
