﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class gestureDebugger : MonoBehaviour {

	public float tappedTime, holdStartedTime, holdCompletedTime, holdLength;
	public bool controlSlider;

	void Start () {
		tappedTime = 0f;
		controlSlider = false;
	}

	void Update () {
		if (Input.GetKeyDown ("d")) {
			HoldStarted ();

		}
	}

	public void Tapped(){
		tappedTime = Time.time;


//		//GetComponent<Text>().text = "Tapped at " + tappedTime + "\n" + "Hold Not Yet ";
	}

	public void HoldStarted(){
		controlSlider = true;
		holdStartedTime = Time.time;
//		GetComponent<Text>().text = "Tapped at " + tappedTime + "\n" + "Hold Started at " + holdStartedTime;


//		//GetComponent<Text>().text = " " + "\n" + "Hold Started at " + holdStartedTime + " Control on: " + controlSlider.ToString();
	}

	public void HoldCompleted(){
		holdCompletedTime = Time.time;
		holdLength = holdCompletedTime - holdStartedTime;

//		//GetComponent<Text>().text = " " + "\n" + "Hold Started at " + holdStartedTime + "\n" + "Hold Completed at " + holdCompletedTime + "\n" + "Hold Length is: " + holdLength;

		controlSlider = false;
	}
}
