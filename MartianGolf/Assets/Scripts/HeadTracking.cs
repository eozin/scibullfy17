﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HeadTracking : MonoBehaviour {

	GameObject mainCam;
	Vector3 initPos;

	void Awake () {
		mainCam = GameObject.FindWithTag ("MainCamera");
		initPos = new Vector3 (mainCam.transform.position.x, mainCam.transform.position.y, mainCam.transform.position.z);
	}


	void Start () {
		
	}

	void Update () {
//		this.GetComponent<Text> ().text = "rotX: " + (mainCam.transform.localRotation.x).ToString () + "\n" + "rotY: " + (mainCam.transform.localRotation.y).ToString () + "\n" +
//			"rotZ: " + (mainCam.transform.localRotation.z).ToString ();

		this.GetComponent<Text> ().text = "posX: " + (mainCam.transform.position.x-initPos.x).ToString () + "\n" + "posY: " + ( mainCam.transform.position.y-initPos.y).ToString () + "\n" +
			"posZ: " + ( mainCam.transform.position.z-initPos.z).ToString ();


	}
}
