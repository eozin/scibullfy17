using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class ClickDebugger : MonoBehaviour, IInputClickHandler, IInputHandler
{


	void Start(){

	}

    public void OnInputClicked(InputClickedEventData eventData)
    {
        // AirTap code goes here
		Debug.Log("OnInputClicked");

    }

    public void OnInputDown(InputEventData eventData)
    {
		Debug.Log("OnInputDown");
    }

    public void OnInputUp(InputEventData eventData)
    {
		Debug.Log("OnInputUp");
    }
}
