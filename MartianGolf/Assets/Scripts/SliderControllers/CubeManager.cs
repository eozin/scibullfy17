using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class CubeManager : MonoBehaviour, IInputClickHandler, IInputHandler
{
	TextMesh debugger;

	void Start(){
		debugger = GetComponentInChildren<TextMesh> ();
		debugger.text = "tap..";
	}

    public void OnInputClicked(InputClickedEventData eventData)
    {
        // AirTap code goes here
		gameObject.transform.eulerAngles = Vector3.zero;
    }

    public void OnInputDown(InputEventData eventData)
    {
		gameObject.transform.Rotate(new Vector3 (5, 15, 0), Space.World);
		debugger.text = "clicked";
    }

    public void OnInputUp(InputEventData eventData)
    {
//		gameObject.transform.Rotate(new Vector3 (0, 0, 0), Space.World);
		gameObject.transform.eulerAngles = Vector3.zero;

		debugger.text = "released";
    }
}
