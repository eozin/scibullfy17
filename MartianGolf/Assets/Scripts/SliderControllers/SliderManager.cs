using HoloToolkit.Unity.InputModule;
using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class SliderManager : MonoBehaviour, IInputClickHandler, IInputHandler
{

	float startTime;
	bool isClicked;
	public GameObject mSlider;
	public gameManager mGameManager;
	public bool isClickableState;
	public bool isPressed;

	void Start(){
		startTime = 0f;
		isClicked = false;
		isClickableState = false;
		isPressed = false;

		mSlider = GameObject.Find("Slider");
	}

	void Update(){

		if (isClicked) {

//			GetComponent<Slider> ().value = (int)(Time.time - startTime); 

//			Debug.Log ((Time.time - startTime)); 
			if (mSlider.GetComponent<AudioSource> () != null) {
				mSlider.GetComponent<AudioSource> ().Play ();
			}

			if ((Time.time - startTime) <= mSlider.GetComponent<Slider> ().minValue) {
				mSlider.GetComponent<Slider> ().value = mSlider.GetComponent<Slider> ().minValue;
			} 
			else {
				mSlider.GetComponent<Slider> ().value = mSlider.GetComponent<Slider> ().maxValue -((Time.time - startTime)*3.5f); 
//				mSlider.GetComponent<Slider> ().value = Time.time - startTime; 
			}
		}
	}

    public void OnInputClicked(InputClickedEventData eventData)
    {
        // AirTap code goes here
    }

    public void OnInputDown(InputEventData eventData) // clicked
    {
		if (isClickableState) {
			if (mGameManager.whichShot == 0) {
				isClicked = true;
				startTime = Time.time;
			}
		}
			
		Debug.Log("OnInputDown");

		isPressed = true;
    }

	public void OnInputUp(InputEventData eventData)  // released
    {
		if (isClickableState) {
			isClicked = false;
			GameObject.Find ("GameManager").SendMessage ("Swing", SendMessageOptions.DontRequireReceiver);
		}

		Debug.Log("OnInputUp");

		isPressed = false;
    }

	IEnumerator PlayBeep(){
		yield return new WaitForSeconds (0.5f);
		mSlider.GetComponent<AudioSource> ().Play ();
		StartCoroutine (PlayBeep ());
	}
}
