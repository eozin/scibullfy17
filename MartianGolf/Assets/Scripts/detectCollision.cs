﻿using UnityEngine;
using System.Collections;

public class detectCollision : MonoBehaviour
{
	public gameManager mGameManager; 
	public audioPlayer mAudioPlayer;

	void OnCollisionEnter (Collision col)
	{

		/*
		if (gameObject.name == "Head") {
			if (col.gameObject.name == "golf_ball_1") {
				Debug.Log ("Hit! - Golf Club");
//				this.GetComponent<AudioSource> ().Play ();
//				this.GetComponent<BallThrough> ().enabled = true;
			}
		}
		*/
			
		if (gameObject.name == "golf_tee") {
			if (mGameManager.state == 1 && col.gameObject.tag == "MainCamera") {
				Debug.Log ("COLLIDED!");
//			Destroy(col.gameObject);
				if (!mGameManager.swingInstructed) {
					mAudioPlayer.SendMessage ("StopAll", SendMessageOptions.DontRequireReceiver);
					mGameManager.GotCloserToTee ();
				}

			}
		}

		/*
		if (gameObject.name == "golf_ball_1") {
			if (col.gameObject.name == "Head") {
				Debug.Log ("Hit!");
				this.GetComponent<AudioSource> ().Play ();
				this.GetComponent<BallThrough> ().enabled = true;
			}
		}
		*/
	}
}