﻿	using HoloToolkit;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;
	using UnityEngine.Windows.Speech;

public class voiceCommands : MonoBehaviour
	{
		float expandAnimationCompletionTime;
		// Store a bool for whether our astronaut model is expanded or not.

		public bool isStartCommandActivated = false;
		public SliderManager mSliderManager;

		// KeywordRecognizer object.
		KeywordRecognizer keywordRecognizer;

		// Defines which function to call when a keyword is recognized.
		delegate void KeywordAction(PhraseRecognizedEventArgs args);
		Dictionary<string, KeywordAction> keywordCollection;

		void Start()
		{
			keywordCollection = new Dictionary<string, KeywordAction>();

			// Add keyword to start manipulation.
//			keywordCollection.Add("Move Astronaut", MoveAstronautCommand);
//			keywordCollection.Add("Expand Model", ExpandModelCommand);

//			keywordCollection.Add("Restart Martian Golf", RestartCommand);

			AddStartCommand ();
//
//			// Initialize KeywordRecognizer with the previously added keywords.
//			keywordRecognizer = new KeywordRecognizer(keywordCollection.Keys.ToArray());
//			keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
//			keywordRecognizer.Start();
		}

		void OnDestroy()
		{
			keywordRecognizer.Dispose();
		}

		private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
		{
			KeywordAction keywordAction;

			if (keywordCollection.TryGetValue(args.text, out keywordAction))
			{
				keywordAction.Invoke(args);
			}
		}

	public void AddStartCommand(){
		keywordCollection.Add("Start", RestartCommand);
//		keywordCollection.Add("Restart Martian Golf", RestartCommand);

		// Initialize KeywordRecognizer with the previously added keywords.
		keywordRecognizer = new KeywordRecognizer(keywordCollection.Keys.ToArray());
		keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
		keywordRecognizer.Start();


	}


	private void RestartCommand(PhraseRecognizedEventArgs args)
	{
		if (mSliderManager.isPressed) {
			isStartCommandActivated = true;
			UnityEngine.SceneManagement.SceneManager.LoadScene (0);
		} else
			return;
	}

	/*
		private void MoveAstronautCommand(PhraseRecognizedEventArgs args)
		{
			GestureManager.Instance.Transition(GestureManager.Instance.ManipulationRecognizer);
		}
	*/

	/*
		private void ExpandModelCommand(PhraseRecognizedEventArgs args)
		{
			// Play animation.  Ensure the Loop Time check box is disabled in the inspector for this animation to play it once.
			Animator[] expandedAnimators = ExpandModel.Instance.ExpandedModel.GetComponentsInChildren<Animator>();
			// Set local variables for disabling the animation.
			if (expandedAnimators.Length > 0)
			{
				expandAnimationCompletionTime = Time.realtimeSinceStartup + expandedAnimators[0].runtimeAnimatorController.animationClips[0].length * 0.9f;
			}

		}
		*/

	void Update(){
		if(Input.GetKeyDown("r"))
		UnityEngine.SceneManagement.SceneManager.LoadScene (0);
	}

	public void Restart(){
		UnityEngine.SceneManagement.SceneManager.LoadScene (0);
	}

	}