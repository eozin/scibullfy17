﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallThrough : MonoBehaviour {

	public float V0, V0x, V0y, theta;
	public float ay, G, Mm, Rm, initT, t;
	public float testDegree, testRadian; 
	public float initX, initY, initZ;

	public float Force, af, afx, afy, mass;

	public bool earth, moon;

	void Awake () {

		Mm = 7.34767309f * Mathf.Pow (10f, 22f); // kilograms
		Rm = 1736.482f;  // kilometer, 1079 mile 

		initT = Time.time;

		testDegree = 60f;

		initX = this.transform.position.x;
		initY = this.transform.position.y;
		initZ = this.transform.position.z;

//		//NEW!
//		af = Force / mass;
//		V0 = af * 0.5f;

//		earth = false; 
//		moon = true;
	}

	void OnEnable() {
		initT = Time.time;
//		theta = mGameManager.mTheta;
		theta = 45;

		//NEW!
//		Force = mGameManager.mForce;
		Force = GameObject.Find ("Slider").GetComponent<Slider> ().value;
		af = Force / mass;
		V0 = af * 0.5f;

		Debug.Log ("Ball Through Script OnEnable");
	}

	void Start () {
//		theta = theta * Mathf.PI;

		ay = G = 1.62f; // m/s2

		V0x = V0 * Mathf.Cos (theta*Mathf.Deg2Rad);
		V0y = V0 * Mathf.Sin (theta*Mathf.Deg2Rad);

		Debug.Log (V0x + " radians");

//		V0x = V0x * Mathf.Rad2Deg;
//		V0y = V0y * Mathf.Rad2Deg;
//
//		Debug.Log (V0x + " degrees");

//		ay = G * (Mm / (Rm * Rm));

		//NEW!
		afx = af * Mathf.Cos (theta*Mathf.Deg2Rad);
		afy = af * Mathf.Sin (theta*Mathf.Deg2Rad);
	}

	void Update () {
		t = Time.time - initT;

		if (moon) {
			earth = false;
			ay = G = 3.711f; //1.62f; //mars - 3.711 m/s²
		}

		if (earth) {
			moon = false;
			ay = G = 9.81f;
		}

//		if (this.transform.position.y > 30.2f) {
		this.transform.position = new Vector3 (initX - (V0x * t), initY + (V0y * t) - (0.5f * ay * (t * t)), initZ - (V0x * t*0.5f));
//		} else {
			
//		}

	}
}
