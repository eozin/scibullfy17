﻿using UnityEngine;
using System.Collections;

public class hololensTracker : MonoBehaviour {

	GameObject mainCam;
	public gameManager mGameManager;
	public audioPlayer mAudioPlayer;

	void Start () {
		mainCam = GameObject.FindWithTag ("MainCamera");
	}

	void Update () {

		float rotX = mainCam.transform.localRotation.x;

		if(mGameManager.state == 0 && mAudioPlayer.isIntroPlayed){
			if (rotX >= 0.265f) {	//looked down		
				mAudioPlayer.PlayWeightOnMars ();
				mGameManager.LookedDown();
			}
		}


		//Unity Editor
		if (Input.GetKeyDown ("a")) {
			mAudioPlayer.PlayWeightOnMars ();
			mGameManager.LookedDown();
		}
	}


}
