﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]

public class audioPlayer : MonoBehaviour {
	
	public AudioSource[] audios;
	public AudioSource[] allAudioSources;
	public bool isIntroPlayed;
	public AudioSource[] feedbackAudios;
	public SliderManager mSliderManager;
	public bool firstShot;

	void Start () {
		audios = GetComponents<AudioSource> ();

		feedbackAudios = GameObject.Find("AudioFeedback").GetComponents<AudioSource> ();

		allAudioSources = FindObjectsOfType (typeof(AudioSource)) as AudioSource[];

		foreach(AudioSource eachAudio in allAudioSources){
			eachAudio.Stop ();
		}

		isIntroPlayed = false;
		firstShot = true;

		PlayIntro ();
	}

	void Update () {
		if (audios [2].isPlaying) {
			audios [1].Pause ();
		}
	}
		
	public void PlayIntro(){
		StartCoroutine (playIntroInRow ());
	}

	IEnumerator playIntroInRow(){
		audios [0].Play ();
		yield return new WaitForSeconds (audios [0].clip.length + 1f);
		audios [1].Play ();
		yield return new WaitForSeconds (audios [1].clip.length - 1.5f);
		isIntroPlayed = true;
	}

	public void PlayWeightOnMars(){
		StopAll ();
		audios [2].Play ();

		StartCoroutine (ActivateClickFunction ());

	}

	public void PlayLookAtHole(){
		audios [3].Play ();
	}

	public void PlayHowToSwing(){
		audios [4].Play ();
	}

	public void PlayThanks(){
		StopAll ();
		audios [5].Play ();
	}

	public void StopAll(){
		allAudioSources = FindObjectsOfType (typeof(AudioSource)) as AudioSource[];

		foreach(AudioSource eachAudio in allAudioSources){
			eachAudio.Stop ();
		}
	}

	IEnumerator ActivateClickFunction(){
		yield return new WaitForSeconds (audios [2].clip.length - 0.5f);
//		GameObject.Find ("golf_tee").GetComponent<BoxCollider> ().enabled = true;
		mSliderManager.isClickableState = true;
	}


	public void PlayBallOnEarth(){
		StartCoroutine (playBallOnEarth ());
	}
		
	IEnumerator playBallOnEarth(){
		StopAll ();
		feedbackAudios [0].Play ();
		yield return new WaitForSeconds (feedbackAudios [0].clip.length);
		feedbackAudios [1].Play ();
	}

	public void PlayTwoArcs(){
		StopAll ();
		feedbackAudios [0].Play ();
	}

	public void PlayLessForce(){
		StopAll ();
		if (firstShot) {
			feedbackAudios [7].Play ();
			firstShot = false;
		} else {
			feedbackAudios [2].Play ();
		}
	}

	public void PlayMoreForce(){
		StopAll ();
		if (firstShot) {
			feedbackAudios [8].Play ();
			firstShot = false;
		} else {
			feedbackAudios [3].Play ();
		}
	}

	public void PlayCorrectForce(){
		StopAll ();
		feedbackAudios [4].Play ();
	}

	public void PlayClickToResetBall(){
		StopAll ();
		feedbackAudios [5].Play ();
		StartCoroutine(activateClickables ());
	}

	public void PlayBallInHole(){
//		StopAll ();
		feedbackAudios [6].Play ();
	}


	IEnumerator activateClickables(){
		yield return new WaitForSeconds (1f);
		mSliderManager.isClickableState = true;
	}

}
