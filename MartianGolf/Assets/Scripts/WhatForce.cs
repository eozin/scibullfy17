﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WhatForce : MonoBehaviour {

	Text whatForce; 

	void Start () {
		whatForce = GetComponentInChildren<Text> ();
	}
	

	void Update () {
		whatForce.text = "Force = " + gameObject.GetComponent<Slider> ().value.ToString();
//		whatForce.color = Color.white;
	}
}
