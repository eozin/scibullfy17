﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class gameManager : MonoBehaviour {

	public int state, whichShot;
	public audioPlayer mAudioPlayer;
	public hololensTracker mHololensTracker;
	public GameObject MarsSet, ball, earthTrail, BallPrefab, EarthTrailPrefab;
	public bool swingInstructed;
	public float mForce;
	public Text feedbackText;
	public int correctForce;

	private bool earthTrailTracking = false;
	private bool marsTrailTracking = false;

	private bool isFirstShotHitted = false;
	public bool stillWaiting = false;

	public SliderManager mSliderManager;


	void Start () {
		state = 0;
		whichShot = 0;
		mForce = 0;
		swingInstructed = false;

		foreach (MeshRenderer eachRenderer in MarsSet.GetComponentsInChildren<MeshRenderer>()) {
			eachRenderer.enabled = false;
		}

		foreach (MeshRenderer eachRenderer in GameObject.Find ("Curiosity").GetComponentsInChildren<MeshRenderer>()) {
			eachRenderer.enabled = false;
		}

		ball.GetComponent<MeshRenderer> ().enabled = false;

		GameObject.Find ("golf_tee").GetComponent<BoxCollider> ().enabled = false;
//
		feedbackText.text = "";
//
		HideSlider ();

	}
	
	 
	void Update () {
		//0. before looking down
		//0.5. [moment] look down 

		//1.1. Looked down: Play audios[2] + FadeInMarsLandscape with Golf Game Set
		//1.2. Timed out. Didn't look down yet

		//1.5. [moment] walked towards the golf tee and got closer enough
		//attach a collider to the camera -> if it hits a trigger near golf tee, Play audios[3]

		// ------------------------------------------------------------------170503 4.44pm DONE

		//2. a - golf club - fade in & play anim
		//   b - Play audios[4]
		//	 c - Show 5 balls UI or show 1/5 number

		if (Input.GetKeyDown ("d")) {
			mAudioPlayer.PlayWeightOnMars ();
			LookedDown();
		}

		if (Input.GetKeyDown ("b")) {
			Swing ();
		}

		if (Input.GetKeyDown ("c")) {
			StartCoroutine (ShowRestartScreen ());
		}

		if (Input.GetKeyDown ("e")) {
			ball.GetComponent<TrailRenderer> ().material.SetColor ("_TintColor", Color.red);
		}

		//3.1. swing(: show trail) -> miss : reaction sound or text + UI text update("n+1/5") 
		//3.2. swing -> goal : reaction sound or text(maybe animate rover?) + hide UI text + status change
		//Update trails : color, transparency change

		if (earthTrailTracking) {
			if (earthTrail.transform.position.y <= -1.25) {
				earthTrail.GetComponentInChildren<BallThrough> ().enabled = false;  //StopBall ();
				earthTrailTracking = false;
			}
		}

		if (marsTrailTracking) {
			if (ball.transform.position.y <= -1.25) {
				//Turn the green arc RED
//				ChangeTrailColor();

//				if (GameObject.Find ("Slider").GetComponent<Slider> ().value > correctForce || GameObject.Find ("Slider").GetComponent<Slider> ().value < correctForce - 2) {
					ball.GetComponent<BallThrough> ().enabled = false; //StopBall ();
					marsTrailTracking = false;
//				}
			}
		}

		//4. Play audios[5] + show text + fade Mars landscape + Golf Game Set away
		//Say "Reset" to reopen the scene

	}

	public void ChangeTrailColor(){
		Slider mmSlider = GameObject.Find ("Slider").GetComponent<Slider> ();

		if (mmSlider.value < correctForce || mmSlider.value > correctForce) {
			ball.GetComponent<TrailRenderer> ().material.SetColor ("_TintColor", new Color (0.98f,0,0,0.5f));
		}

	}


	public void Swing(){
				
		whichShot ++;

		if (whichShot == 1) {
//			GameObject.FindWithTag ("GolfClub").GetComponent<Animator> ().enabled = true; //PlayGolfClubAnim()
			GameObject.FindWithTag ("GolfClub").GetComponent<Animator> ().Play("GolfClubSwingForward", -1, 0);
			StartCoroutine (HitBall()); 
		}

		/*
		if (whichShot == 2) {
//			GameObject.FindWithTag ("GolfClub").GetComponent<Animator> ().enabled = false;
//			GameObject.FindWithTag ("GolfClub").GetComponent<Animator> ().enabled = true; //PlayGolfClubAnim()
			GameObject.FindWithTag ("GolfClub").GetComponent<Animator> ().Play("GolfClubSwingForward", -1, 0);
//			GameObject.FindWithTag ("GolfClub").GetComponent<Animator> ().Play("GolfClubBackToNormal", -1, 0);
			StartCoroutine (HitBall()); 
		}
		*/
			
		if (whichShot > 1) {
			whichShot = 0;
			stillWaiting = false;
			GameObject.FindWithTag ("GolfClub").GetComponent<Animator> ().Play("GolfClubBackToNormal", -1, 0);
			StartCoroutine (ResetHitSetting ());
		}
	}

	public void LookedDown(){
		state = 1;
		StartCoroutine (FadeInMars ());
		//TimeOut Marker Here
	}

	public void Reset(){
		state = 0;
		mAudioPlayer.isIntroPlayed = false;
		StartCoroutine (FadeOutMars ());
	}

	IEnumerator FadeInMars(){
//		yield return new WaitForSeconds (mAudioPlayer.audios[2].clip.length - 7f);
		yield return new WaitForSeconds (9f);
		Debug.Log ("Fade In Mars");

//		mSliderManager.isClickableState = true;
			
		foreach (MeshRenderer eachRenderer in MarsSet.GetComponentsInChildren<MeshRenderer>()) {
			eachRenderer.enabled = true;
		}
			
		foreach (MeshRenderer eachRenderer in GameObject.Find ("Curiosity").GetComponentsInChildren<MeshRenderer>()) {
			eachRenderer.enabled = true;
		}

		ball.GetComponent<MeshRenderer> ().enabled = true;


//		GameObject.Find ("arrow_tee").GetComponentInChildren<MeshRenderer> ().enabled = false;
		//Enable the Arrow to the Golf Tee
//		yield return new WaitForSeconds (2f);

		/*
		foreach (MeshRenderer eachCRenderer in GameObject.Find ("arrow_tee").GetComponentsInChildren<MeshRenderer> ()) {
//			GameObject.Find ("arrow_tee").GetComponentInChildren<MeshRenderer> ().enabled = true;
			eachCRenderer.enabled = true;
		}
		*/

//		GameObject.Find ("arrow_tee").GetComponentInChildren<Animator> ().enabled = true;

		ShowSlider ();
	}

	IEnumerator FadeOutMars(){
		Debug.Log ("Fade Out Mars");
		yield return new WaitForSeconds (1f);
	}
		
	public void GotCloserToTee(){
		foreach (MeshRenderer eachCRenderer in GameObject.Find ("arrow_tee").GetComponentsInChildren<MeshRenderer> ()) {
			//			GameObject.Find ("arrow_tee").GetComponentInChildren<MeshRenderer> ().enabled = false;
			eachCRenderer.enabled = false;
		}
//		GameObject.Find ("arrow_tee").GetComponentInChildren<Animator> ().enabled = false;

		state = 2; 
		swingInstructed = true;

		mAudioPlayer.PlayHowToSwing ();
		//show a golf club
	} 
		

	IEnumerator HitBall(){

			mSliderManager.isClickableState = false;

			mForce = GameObject.Find ("Slider").GetComponent<Slider> ().value;
			yield return new WaitForSeconds (0.26f);
			ball.GetComponent<AudioSource> ().Play (); //hit sound

		if (whichShot == 1) {
			earthTrail.GetComponentInChildren<BallThrough> ().enabled = true;
			earthTrailTracking = true;

			ball.GetComponentInChildren<BallThrough> ().enabled = true;
			marsTrailTracking = true;

			stillWaiting = true;



			yield return new WaitForSeconds (0.5f);

			if (!isFirstShotHitted) {
//				GameObject.Find ("VoiceCommands").SendMessage ("AddStartCommand");
				mAudioPlayer.PlayTwoArcs (); //mAudioPlayer.PlayBallOnEarth ();
				StartCoroutine (ShowResultFeedback ());
				isFirstShotHitted = true;
			} else {
				StartCoroutine (ShowResultFeedback ());
			}
				
//			StartCoroutine (ShowResultFeedback());
//			GameObject.FindWithTag ("GolfClub").GetComponent<Animator> ().Play("GolfClubBackToNormal", -1, 0);
		}

		/*
		if (whichShot == 2) {
//			ball.GetComponentInChildren<BallThrough> ().enabled = true;
//			marsTrailTracking = true;
			StartCoroutine (ShowResultFeedback());
		}
		*/

	}

	IEnumerator ShowResultFeedback(){
		Slider mSlider = GameObject.Find ("Slider").GetComponent<Slider> ();

		if (!isFirstShotHitted) {
			yield return new WaitForSeconds (mAudioPlayer.feedbackAudios [0].clip.length + 0.5f);
		} else {
			yield return new WaitForSeconds (2.3f);
		}

		if (mSlider.value < correctForce) {

//			feedbackText.text = "Try again with more force";
			if (stillWaiting) mAudioPlayer.PlayMoreForce ();

				yield return new WaitForSeconds (2f);

//			feedbackText.text = "Click once to reset ball";
			if (stillWaiting) {
				mAudioPlayer.PlayClickToResetBall ();
			}

		} 
			
		if (mSlider.value > correctForce) {
//			feedbackText.text = "Try again with less force";
			if (stillWaiting) mAudioPlayer.PlayLessForce ();

			yield return new WaitForSeconds (2f);

//			feedbackText.text = "Click once to reset ball";
			if (stillWaiting) {
				mAudioPlayer.PlayClickToResetBall ();
			}

//			GameObject.FindWithTag ("GolfClub").GetComponent<Animator> ().Play("GolfClubBackToNormal", -1, 0);
		} 
			
//		/*
		if (mSlider.value == correctForce) {
			mSliderManager.isClickableState = false;
//			mAudioPlayer.PlayBallInHole ();
			yield return new WaitForSeconds (0.55f);
//			feedbackText.text = "You did it!!";
			GameObject.Find ("Curiosity").GetComponent<Animator> ().enabled = true;
			mAudioPlayer.PlayCorrectForce ();


//			StartCoroutine (ShowRestartScreen());
			StartCoroutine (FadeOutEntireScene());

		} 
//		*/

//		yield return new WaitForSeconds (2f);
//
//		feedbackText.text = "Click once to reset ball";
//		mAudioPlayer.PlayClickToResetBall ();

	}

	public void ShowHoleIn(){
//		StartCoroutine (HoleIn ());
		mAudioPlayer.PlayBallInHole ();
	}

	IEnumerator HoleIn(){
		mAudioPlayer.PlayBallInHole ();
		yield return new WaitForSeconds (0.5f);
	}

	IEnumerator ShowRestartScreen(){
		yield return new WaitForSeconds (5f);
		HideSlider ();

		feedbackText.text = "";
		mAudioPlayer.PlayThanks ();

		GameObject restartPanel = GameObject.Find ("RestartPanel");

		//fadeInPanel
		for (float f = 0; f <= 0.765f; f+=0.03f) {
			Color c1 = restartPanel.GetComponent<Image>().color;
			c1.a = f;
			restartPanel.GetComponent<Image> ().color = c1;
			yield return new WaitForSeconds (0.03f);
		}

		//fadeInText
		Text[] txt = restartPanel.GetComponentsInChildren<Text> ();
		for (float f = 0; f < 1f; f+=0.03f) {
			Color color = txt [0].color;
//			Color c2 = restartPanel.transform.GetChild(1).gameObject.GetComponent<Image>().color;
			color.a = f;
//			c2.a = f;

			for(int i = 0; i < txt.Length; i++){
				txt [i].color = color;
			}

//			restartPanel.transform.GetChild(1).gameObject.GetComponent<Image>().color = c2;	
			yield return new WaitForSeconds (0.03f);
		}
	}

	//-------------------------------------------------------------------
	IEnumerator FadeOutEntireScene(){

		yield return new WaitForSeconds (5f);
		Debug.Log ("Fade Out the ENTIRE SCENE");

		mSliderManager.isClickableState = false;

		HideSlider ();

		feedbackText.text = "";
		mAudioPlayer.PlayThanks ();

		GameObject restartPanel = GameObject.Find ("RestartPanel");

		//fadeInPanel
		for (float f = 0; f <= 1f; f+=0.03f) {
			Color c1 = restartPanel.GetComponent<Image>().color;
			c1.a = f;
			restartPanel.GetComponent<Image> ().color = c1;
			yield return new WaitForSeconds (0.03f);
		}

		/*
		foreach (MeshRenderer eachRenderer in MarsSet.GetComponentsInChildren<MeshRenderer>()) {
			
			eachRenderer.enabled = true;
		}

		foreach (MeshRenderer eachRenderer in GameObject.Find ("Curiosity").GetComponentsInChildren<MeshRenderer>()) {
			eachRenderer.enabled = true;
		}

		ball.GetComponent<MeshRenderer> ().enabled = true;

		HideSlider ();
		*/
	}	
	//-------------------------------------------------------------------


	IEnumerator ResetHitSetting(){
//		yield return new WaitForSeconds (4f);

		feedbackText.text = "";
		mAudioPlayer.StopAll ();

		GameObject.Find ("Slider").GetComponent<Slider> ().value = GameObject.Find ("Slider").GetComponent<Slider> ().maxValue;

		GameObject[] currentBalls = GameObject.FindGameObjectsWithTag ("GolfBall");

		for (int i = 0; i < currentBalls.Length; i++) {
			Destroy (currentBalls[i]);
		}

//		yield return new WaitForSeconds (0.5f);
		GameObject newBall = (GameObject)Instantiate (BallPrefab);

		GameObject newETrail = (GameObject)Instantiate (EarthTrailPrefab);

		ball = newBall;
		earthTrail = newETrail;

		yield return new WaitForSeconds (0.5f);
	}

	public void HideSlider(){
		GameObject mSlider = GameObject.Find ("Slider");

		foreach (Image eachImage in mSlider.GetComponentsInChildren<Image>()) {
			eachImage.enabled = false;
		}

		mSlider.GetComponentInChildren<Text> ().enabled = false;
	}

	public void ShowSlider(){
		GameObject mSlider = GameObject.Find ("Slider");

		foreach (Image eachImage in mSlider.GetComponentsInChildren<Image>()) {
			eachImage.enabled = true;
		}

		mSlider.GetComponentInChildren<Text> ().enabled = true;
	}
}
