﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallInHoleDetection : MonoBehaviour {

	public gameManager mGameManager; 
//	public audioPlayer mAudioPlayer;

	private bool playOnce = false;

	void OnCollisionEnter (Collision col)
	{
		if (col.gameObject.tag == "GolfBall") {
			if(!playOnce){
				Debug.Log ("Hole In!! :)");
				mGameManager.SendMessage ("ShowHoleIn", SendMessageOptions.DontRequireReceiver);
				playOnce = true;
			}
		}
			
	}
}
 